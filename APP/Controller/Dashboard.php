<?php 

class Dashboard {

	public function __construct() {
		if (! is_numeric(Session()->get('aid'))) {
			redirect('/Index/home/');
		}
	}

	public function index() {

		$data['list']	= call('Model/ModelLog')->get_list(['page' => 1, 'log_status' => 1]);

		includePage('home', false, $data);
	}

}