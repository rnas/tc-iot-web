<?php 

class Logs {

	public function __construct() {
		if (! is_numeric(Session()->get('aid'))) {
			redirect('/Index/home/');
		}
	}

	private $status = [
	1 	=> 'Acesso Liberado',
	2 	=> 'Apresentou HASH Antigo',
	4 	=> 'Apresentou HASH Inválido',
	];

	public function index() {

		$page = getData('page', 1);

		if (is_numeric($page)) {
			
			$data['list']	= call('Model/ModelLog')->get_list(['page' => $page]);
			$data['count'] 	= call('Model/ModelLog')->get_count([]);
			$data['pager']	= call('Helper/Paginator')->go($page, $data['count'], '/Logs/index/page/');
			$data['status'] = $this->status;

		}

		includePage('home', 'Logs', $data);
	}

	public function notFound() {
		$this->index();
	}
}