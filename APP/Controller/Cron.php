<?php 

class Cron {

	public function generate() {
		
		// pattern
		// user_id:md5(hash_data):md5(random)

		$people = call('Model/ModelPeople')->get_list([]);

		foreach ($people as $user) {
			
			$key = $user->people_id . md5( $user->people_in . $user->people_out . $user->people_slack ) . md5(rand(0,100000000));
			$key = md5($key);

			call('Model/ModelHash')->add_hash([
				'people_id' => $user->people_id,
				'hash_in' 	=> $user->people_in,
				'hash_out' 	=> $user->people_out,
				'hash_key'  => $key
			]);

			if ($user->people_token) {
				call('Helper/Push')->withContent('Ei, ' . $user->people_name . '!', 'Seus códigos de acesso foram gerados! Clique em ver meus acessos para visualizar.');
				call('Helper/Push')->sendTo($user->people_token);
			}

			$qr_code = 'https://chart.googleapis.com/chart?cht=qr&chs=500x500&chl=' . $key;	

			call('Helper/SlackAPI')->sendMessage(
				$qr_code,
				':door:',
				'[porteiro]',
				'@' . $user->people_slack
			);
		}
	}
} ?>