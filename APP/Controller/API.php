<?php 

class API {

	public function hash() {
		
		// pattern
		// hash_id:md5(hash_data):md5(random)

		$hash = getData('hash', false);

		if (!$hash) {
			return $this->e400();
		}

		if (! ctype_alnum($hash)) {
			return $this->e400();
		}

		$valid = call('Model/ModelHash')->get_by(['hash_key' => $hash]);

		if (! $valid) {
			return $this->e401($valid, 2);
		}

		if (($valid->hash_in > date('G')) || ($valid->hash_out < date('G'))) {
			return $this->e401($valid, 4);	
		}

		call('Model/ModelHash')->edit_hash(['hash_uses' => $valid->hash_uses + 1], ['hash_id' => $valid->hash_id]);
		$people = call('Model/ModelPeople')->get_by_id($valid->people_id);

		$this->success($valid, $people);

	}


	public function sync() {
		
		$hash = getData('sync', false);

		if ($hash) {
			
			$data = call('Model/ModelPeople')->get_by(['people_hash' => $hash]);

			if ($data) {
				echo json_encode($data);
			} else {
				$this->e404();
			}
		} else {
			$this->e400();
		}

	}

	// public function test(){
		
	// 	call('Helper/Push')->withContent('ola', 'como vai');
	// 	call('Helper/Push')->sendTo('f9EQSba6OvA:APA91bGQjYN0laX5Sby-hqACk7YCA9xMfy-cAtokubudBWHrZ0HwQ-1iegaXL1gCS2Edar_R6Kmin8RiGxUDAfAjmg4YhQ21A-S1a40y4-hSTaWCrg46eTUXai0iNb2gEHdEhXptmRi5');

	// }

	public function token() {
		
		$token = getData('token');
		$hash  = getData('hash');

		if ($token && $hash) {
			
			$data = call('Model/ModelPeople')->get_by(['people_hash' => $hash]);

			if ($data) {
				call('Model/ModelPeople')->edit_people(['people_token' => $token], ['people_id' => $data->people_id]);
				echo json_encode(['success' => true]);
			} else {
				$this->e404();
			}

		} else {
			$this->e400();
		}

	}

	public function qr() {

		$hash = getData('from', false);

		if ($hash) {
			
			$data = call('Model/ModelPeople')->get_by(['people_hash' => $hash]);

			if ($data) {
				
				$hash = call('Model/ModelHash')->get_last_by(['people_id' => $data->people_id]);
				echo json_encode($hash);

			} else {
				$this->e404();
			}
		} else {
			$this->e400();
		}
		
	}


	public function success($valid, $people) {
		call('Model/ModelLog')->add_log([
			'user_id' => $valid->people_id,
			'log_status' => 1
		]);

		call('Helper/SlackAPI')->sendMessage(
			$people->people_name . ' acabou de entrar.',
			':door:',
			'[porteiro]',
			'#porta'
		);

		echo json_encode(['success' => true]);
	}

	public function e400() {
		header("HTTP/1.1 400 Malformed Request");
		echo json_encode(['success' => false, 'error' => 'no hash']);
	}

	public function e404() {
		header("HTTP/1.1 404 Not Found");
	}

	public function e401($valid, $error) {
		call('Model/ModelLog')->add_log([
			'user_id' => $valid->people_id,
			'log_status' => $error
		]);

		header("HTTP/1.1 401 Malformed Unauthorized");
		echo json_encode(['success' => false, 'error' => 'unauthorized']);
	}

} ?>