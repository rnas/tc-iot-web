<?php 

class Pessoas {

	public function __construct() {
		if (! is_numeric(Session()->get('aid'))) {
			redirect('/Index/home/');
		}
	}

	public function index() {

		$page = getData('page', 1);

		if (is_numeric($page)) {
			
			$data['list']	= call('Model/ModelPeople')->get_list(['page' => $page]);
			$data['count'] 	= call('Model/ModelPeople')->get_count([]);
			$data['pager']	= call('Helper/Paginator')->go($page, $data['count'], '/People/index/page/');

		}

		includePage('home', 'Pessoas', $data);
	}

	public function adicionar($result = false) {
		$data = ['result' => $result];
		includePage('edit', false, $data);
	}

	public function editar($result = false) {

		$id = getData('editar', false);
		if ((! $id) && (is_numeric(@$result['id']))) {
			$id = $result['id'];
		}

		if (is_numeric($id)) {
			$data['result'] = $result;
			$data['form']	= call('Model/ModelPeople')->get_by(['people_id' => $id]);
			includePage('edit', false, $data);
		} else {
			$this->adicionar();	
		}
	}



	public function _save() {
		
		$data = params(['req' => [
			'people_name',
			'people_job',
			'people_photo',
			'people_in',
			'people_out',
			'people_slack',
		]], false, [
			'people_status' => 1,
		]);

		$id = postData('people_id', false);

		if($data) {

			if (is_numeric($id)) {
				call('Model/ModelPeople')->edit_people($data, ['people_id' => $id]);
				$this->editar(['success' => 'Pessoa editada com sucesso', 'id' => $id]);
			} else {

				$key = $data['people_name'] . md5( $data['people_name'] ) . md5(rand(0,100000000));
				$key = md5($key);

				$data['people_hash'] = $key;
				call('Model/ModelPeople')->add_people($data);
				$this->adicionar(['success' => 'Pessoa adicionada com sucesso.']);
			}

		} else {
			$this->adicionar(['error' => 'Erro: Prencha todos os campos obrigatórios abaixo']);
		}

	}

}