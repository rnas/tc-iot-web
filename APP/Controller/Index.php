<?php

Class Index {

	public function home(){

		if (is_numeric(Session()->get('aid'))) {
			redirect('/Dashboard/index/');	
		}

		$this->login();
	}

	public function loginsubmit(){
		$user = params(array('req' => array('user|user_username', 'pass|user_hash')));
		$redirect = postData('redirect', false);

		// Recebeu os parametros
		if ($user) {

			// Efetua hash da senha do usuário
			$user['user_hash'] = call('Helper/Hash')->encode($user['user_hash']);

			// Apenas usertype admin
			$user['user_type'] = '1';

			// Consulta usuário e senha na base de dados
			$user = call('Model/ModelUser')->get_by($user);

			if ($user) {
				// Encontrou: Cria session para autenticação do usuário
				Session()->set('aid', $user->user_id);
				// Retorno	
				redirect('/Dashboard/index/');		
				
			} else {
				// Redirect
				$this->login(['error' => 'Usuário ou senha incorretos.']);
			}

		} else {
			$this->login(['error' => 'Preencha os campos usuário e senha.']);
		}
	}

	public function login($error = false) {
		$return['result'] = $error;
		includePage('login', 'Index', $return);	
		
	}	

	public function logout() {
		Session()->removeAll();
		$this->login();
	}

	public function notFound() {
		
	}
}