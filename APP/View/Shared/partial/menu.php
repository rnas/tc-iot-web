<ul id="slide-out" class="side-nav fixed z-depth-2">
		<li class="center no-padding">
			<div class="indigo darken-2 white-text" style="height: 180px;">
				<div class="row">
					<img style="margin-top: 5%;" width="100" height="100" src="/imgs/porteiro.png" class="circle responsive-img" />

					<p style="margin-top: -8%;">
						<strong>Porteiro</strong>
					</p>
				</div>
			</div>
		</li>


		<ul class="collapsible" data-collapsible="accordion">
			<li id="dash_users">
				<div id="dash_users_header" class="collapsible-header waves-effect"><b>Pessoas</b></div>
				<div id="dash_users_body" class="collapsible-body">
					<ul>
						<li id="users_seller">
							<a class="waves-effect" style="text-decoration: none;" href="/Pessoas/adicionar">Adicionar</a>
						</li>

						<li id="users_customer">
							<a class="waves-effect" style="text-decoration: none;" href="/Pessoas/index">Listar</a>
						</li>
					</ul>
				</div>
			</li>

			<li id="dash_products">
				<div id="dash_products_header" class="collapsible-header waves-effect"><b>Logs</b></div>
				<div id="dash_products_body" class="collapsible-body">
					<ul>
						<li id="products_product">
							<a class="waves-effect" style="text-decoration: none;" href="/Logs">Ver todos</a>
						</li>
					</ul>
				</div>
			</li>

			<li id="dash_brands">
				<div id="dash_brands_header" class="collapsible-header waves-effect"><b>Preferências</b></div>
				<div id="dash_brands_body" class="collapsible-body">
					<ul>
						<li id="brands_brand">
							<a class="waves-effect" style="text-decoration: none;" href="#!">Meu Acesso</a>
						</li>

						<li id="brands_sub_brand">
							<a class="waves-effect" style="text-decoration: none;" href="/Index/logout">Logout</a>
						</li>
					</ul>
				</div>
			</li>
		</ul>
	</ul>


	<header>
		<ul class="dropdown-content" id="user_dropdown">
			<li><a class="indigo-text" href="#!">Profile</a></li>
			<li><a class="indigo-text" href="/Index/logout">Logout</a></li>
		</ul>

		<nav class="indigo" role="navigation">
			<div class="nav-wrapper">
				<a data-activates="slide-out" class="button-collapse show-on-" href="#!"><img style="margin-top: 17px; margin-left: 5px;" src="https://res.cloudinary.com/dacg0wegv/image/upload/t_media_lib_thumb/v1463989873/smaller-main-logo_3_bm40iv.gif" /></a>

				<ul class="right hide-on-med-and-down">
					<li>
						<a class='right dropdown-button' href='' data-activates='user_dropdown'><i class=' material-icons'>account_circle</i></a>
					</li>
				</ul>

				<a href="#" data-activates="slide-out" class="button-collapse"><i class="mdi-navigation-menu"></i></a>
			</div>
		</nav>

		<nav>
			<div class="nav-wrapper indigo darken-2">
				<a style="margin-left: 20px;" class="breadcrumb" href="#!">Porteiro</a>
				<a class="breadcrumb" href="#!">Home</a>

				<div style="margin-right: 20px;" id="timestamp" class="right"></div>
			</div>
		</nav>
	</header>