<!Doctype html>

<html lang="pt-BR">
<head>
	<meta charset="utf-8">
	<!-- TODO: keywords -->
	<title>Porteiro</title>
	<meta name="keywords" content="" />

	<meta http-equiv="X-UA-Compatible" content="IE=edge">


	<?php if (isMobile()) { ?>	

	<?php if ((bool)(preg_match('/Firefox/i', $_SERVER['HTTP_USER_AGENT']))) { ?>
	<meta name="viewport" content="width=640"/>
	<?php } else if ((bool)(preg_match('/Windows Phone/i', $_SERVER['HTTP_USER_AGENT']))) { ?>
	<meta name="viewport" content="width=640"/>
	<?php } else { ?>
	<meta name="viewport" content="width=640, user-scalable=0"/>
	<?php } ?>

	<?php } else { ?>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php } ?>

	<!-- arquivos de css -->
	<link rel="shortcut icon" href="" type="image/x-icon" />
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/css/materialize.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/font/material-design-icons/Material-Design-Icons.woff">
	
	<script type="text/javascript" src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/js/materialize.min.js"></script>
	


<?php getCs(); ?>

<!-- arquivos de javascript -->
<script src="/js/jquery.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<!-- Just to make our placeholder images work. Don't actually copy the next line! -->
<!-- <script src="../../assets/js/vendor/holder.min.js"></script> -->
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<!-- <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script> -->
<?php getJs(); ?>

</head>
<body>