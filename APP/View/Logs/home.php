<?php includePartial('header', 'Shared') ?>

 <style type="text/css">
    header,
main,
footer {
  padding-left: 240px;
}

body {
  backgroud: white;
}

@media only screen and (max-width: 992px) {
  header,
  main,
  footer {
    padding-left: 0;
  }
}

#credits li,
#credits li a {
  color: white;
}

#credits li a {
  font-weight: bold;
}

.footer-copyright .container,
.footer-copyright .container a {
  color: #BCC2E2;
}

.fab-tip {
  position: fixed;
  right: 85px;
  padding: 0px 0.5rem;
  text-align: right;
  background-color: #323232;
  border-radius: 2px;
  color: #FFF;
  width: auto;
}
  </style>

<body>
	

	<?php includePartial('menu', 'Shared', array('page' => 'donations')); ?>
	<main>

		<div class="row">
			<div class="col s12">
				<h3>Ações</h3>
				<br>
				<div class="mdc-form-field">
					<select>
						<option>Filtros</option>
						<option>Filtros</option>
					</select>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col s12">

				<?php foreach ($Data['list'] as $log): ?>
					
					<div style="padding: 5px; border-left: solid 5px <?php echo ($log->log_status == 1) ? 'green' : 'red'; ?>" class="card">
					<div class="row">
						<div class="col s2">
							<div class="center">
								<img src="/imgs/placeholder.png" align="center" style="margin-top: 20px" class="circle responsive-img" width="60px" />
							</div>
						</div>
						<div class="col s8">
							<div class="left card-title">
								<b><?php echo $log->people_name ?></b><br />
								<small><?php echo $log->people_job ?></small><br>
								<small><?php echo $Data['status'][$log->log_status] ?></small>
							</div>
						</div>

						<div class="col s2" style="padding-top: 20px; ">

							<span data-badge-caption="a"><?php echo $log->log_created ?></span>

						</div>

					</div>
				</div>

				<?php endforeach ?>

				<center><?php echo $Data['pager'] ?></center>

			</div>

		</div>


	</main>

</body>

</html>
