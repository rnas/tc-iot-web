<?php includePartial('header', 'Shared') ?>

 <style type="text/css">
    header,
main,
footer {
  padding-left: 240px;
}

body {
  backgroud: white;
}

@media only screen and (max-width: 992px) {
  header,
  main,
  footer {
    padding-left: 0;
  }
}

#credits li,
#credits li a {
  color: white;
}

#credits li a {
  font-weight: bold;
}

.footer-copyright .container,
.footer-copyright .container a {
  color: #BCC2E2;
}

.fab-tip {
  position: fixed;
  right: 85px;
  padding: 0px 0.5rem;
  text-align: right;
  background-color: #323232;
  border-radius: 2px;
  color: #FFF;
  width: auto;
}
  </style>

<body>

		<?php includePartial('menu', 'Shared', array('page' => 'orgs')); ?>
		
		<main>
   
   <div class="row">
     <div class="col s12">
       <h3>Pessoas</h3>
     </div>
   </div>

    <div class="row">
      <div class="col s12">


        <?php foreach ($Data['list'] as $people): ?>
          <div style="padding: 15px;" class="card">
          <div class="row">
            <div class="col s2">
              <div class="center">
              <img src="imgs/placeholder.png" align="center" class="circle responsive-img" width="90px" />
              </div>
            </div>
            <div class="col s8">
              <div class="left card-title">
              <b><?php echo $people->people_name ?></b><br />
              <small><?php echo $people->people_job ?></small><br>
              <small>Acesso: Seg - Sex, das <?php echo $people->people_in ?>h às <?php echo $people->people_out ?>h</small>
            </div>
            </div>

             <div class="col s2">
              <a href="/Pessoas/editar/<?php echo $people->people_id ?>" class="btn btn-default btn-block">Editar</a>
            </div>
            
          </div>
        </div>
        <?php endforeach ?>

        


      </div>

    </div>

    <div class="fixed-action-btn click-to-toggle" style="bottom: 45px; right: 24px;">
      <a class="btn-floating btn-large pink waves-effect waves-light">
        <i class="large material-icons">add</i>
      </a>

      <ul>
     
        <li>
          <a class="btn-floating blue"><i class="material-icons">vpn_key</i></a>
          <a href="/Pessoas/adicionar" class="btn-floating fab-tip">Adicionar acesso</a>
        </li>
      </ul>
    </div>
  </main>


	</body>
	
	</html>
