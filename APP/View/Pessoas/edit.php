<?php includePartial('header', 'Shared') ?>

<style type="text/css">
header,
main,
footer {
	padding-left: 240px;
}

body {
	backgroud: white;
}

@media only screen and (max-width: 992px) {
	header,
	main,
	footer {
		padding-left: 0;
	}
}

#credits li,
#credits li a {
	color: white;
}

#credits li a {
	font-weight: bold;
}

.footer-copyright .container,
.footer-copyright .container a {
	color: #BCC2E2;
}

.fab-tip {
	position: fixed;
	right: 85px;
	padding: 0px 0.5rem;
	text-align: right;
	background-color: #323232;
	border-radius: 2px;
	color: #FFF;
	width: auto;
}
</style>

<body>

	<?php includePartial('menu', 'Shared', array('page' => 'orgs')); ?>

	<main>

		<div class="row">
			<div class="col s12">
				<h3>Adicionar Pessoa</h3>
			</div>
		</div>

		<div class="row">
			<div class="col s12">
				<div style="padding: 15px;" class="card">
					<div class="row">
						<div class="col s2">
							<div class="center">
								<img src="/imgs/placeholder.png" align="center" class="circle responsive-img" width="90px" />
							</div>
						</div>
						<div class="col s8">

							<?php if (@$Data['result']['success']): ?>
						<div class="alert alert-success" role="alert">
							<?php echo $Data['result']['success'] ?>
						</div>
					<?php elseif (@$Data['result']['error']): ?>
						<div class="alert alert-danger" role="alert">
							<?php echo $Data['result']['error'] ?>
						</div>
					<?php endif ?>

					<form method="post" action="/Pessoas/_save/">

						<?php if (@$Data['form']): ?>
									<input type="hidden" name="people_id" value="<?php echo $Data['form']->people_id ?>" />
								<?php endif ?>

							<div class="mdc-form-field">
								<input type="text" name="people_name" value="<?php echo @$Data['form']->people_name ?>" placeholder="Nome">
							</div>

							<div class="mdc-form-field">
								<input type="text" name="people_job" value="<?php echo @$Data['form']->people_job ?>" placeholder="Cargo">
							</div>

							<div class="mdc-form-field">
								<input type="text" name="people_photo" value="<?php echo @$Data['form']->people_photo ?>" placeholder="URL Foto">
							</div>

							<div class="mdc-form-field">
								<input type="text" name="people_slack" value="<?php echo @$Data['form']->people_slack ?>" placeholder="Username do Slack">
							</div>

							<h4>Acessos</h4>

							<div class="row">
								<div class="col s4"><strong>Entrada</strong></div>
								<div class="col s4"><strong>Saída</strong></div>
							</div>

							<div class="row">
								<div class="col s4"> <input type="text" name="people_in" value="<?php echo @$Data['form']->people_in ?>" placeholder="hora de entrada" name=""> </div>
								<div class="col s4"> <input type="text" name="people_out" value="<?php echo @$Data['form']->people_out ?>" placeholder="hora de saída" name=""> </div>

							</div>

							<?php if (@$Data['form']): ?>
								<h4>Hash de Acesso</h4>
								<img src="https://chart.googleapis.com/chart?cht=qr&chs=500x500&chl=<?php echo @$Data['form']->people_hash ?>">
							<?php endif ?>

							

							<input type="submit" name="" class="btn btn-default btn-block" value="SALVAR" />

							</form>


						</div>

					</div>
				</div>

			</div>

		</div>

	</main>


</body>

</html>
