<?php includePartial('header', 'Shared'); ?>
<style type="text/css">


#credits li,
#credits li a {
	color: white;
}

#credits li a {
	font-weight: bold;
}

.footer-copyright .container,
.footer-copyright .container a {
	color: #BCC2E2;
}

body {
	background: #3F51B5;
}

.fab-tip {
	position: fixed;
	right: 85px;
	padding: 0px 0.5rem;
	text-align: right;
	background-color: #323232;
	border-radius: 2px;
	color: #FFF;
	width: auto;
}
</style>
<body>


	<main id="show">

		<div class="row">

			<div class="col m4">&nbsp;</div>

			<div class="col s4" style="margin-top: 150px">
				<div style="padding: 15px;" class="card">
					<div class="row">
						<div class="col s2">
							<div class="center">
								<img src="/imgs/placeholder.png" align="center" class="circle responsive-img" width="90px" />
							</div>
						</div>
						<div class="col s10">
							<div class="left card-title">
								<b>Login</b><br />
								<small>Acesse sua conta</small>
							</div>

						</div>

					</div>

					<?php if (@$Data['result']['error']): ?>
						<div class="alert alert-danger" role="alert">
							<?php echo $Data['result']['error'] ?>
						</div>
						<hr />
					<?php endif ?>

					<form class="form-signin" role="form" method="post" action="/Index/loginsubmit/">

						<div class="mdc-form-field">
							<input type="text" name="user" placeholder="Username" />
						</div>

						<div class="mdc-form-field">
							<input type="password" name="pass" placeholder="Senha" />
						</div>

						<div class="mdc-form-field">
							<input type="checkbox" id="input">
							<label for="input">Permanecer Logado</label>
						</div>


						<input type="submit" class="btn right" value="Fazer Login" />

					</form>

				</div>


			</div>

		</div>

	</main>

</body>
<script type="text/javascript">
	$(document).ready(function(){
		$('#show').hide();
		$('#show').removeClass('hidden');
		$('#show').fadeIn(800);

	})
</script>
</html>