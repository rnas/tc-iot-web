<?php 

Class acesso {

	private $db = array(
		'production' 	=> array(
			'host' => '****',
			'user' => '****',
			'password' => '',
			'database' => 'porteiro'
		),
		'testing' 		=> array(
			'host' => '****',
			'user' => '****',
			'password' => '',
			'database' => 'porteiro'
		),
		'development' 	=> array(
			'host' => '****',
			'user' => '****',
			'password' => '',
			'database' => 'porteiro'
		),
	);

	public function getdb($ambient = 'development'){
		return $this->db[$ambient];
	}

}