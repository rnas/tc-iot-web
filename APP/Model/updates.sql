 create database porteiro;

 CREATE TABLE `porteiro`.`user` (
  `user_id` INT NOT NULL AUTO_INCREMENT,
  `user_username` VARCHAR(45) NOT NULL,
  `user_hash` VARCHAR(256) NOT NULL,
  `user_type` INT NOT NULL DEFAULT 1,
  PRIMARY KEY (`user_id`));

CREATE TABLE `porteiro`.`people` (
  `people_id` INT NOT NULL AUTO_INCREMENT,
  `people_name` VARCHAR(45) NOT NULL,
  `people_job` VARCHAR(45) NOT NULL,
  `people_photo` VARCHAR(128) NOT NULL,
  `people_status` INT NOT NULL DEFAULT 1,
  PRIMARY KEY (`people_id`));

CREATE TABLE `porteiro`.`log` (
  `log_id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT NOT NULL,
  `log_status` INT NOT NULL DEFAULT 1,
  `log_created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`log_id`));

ALTER TABLE `porteiro`.`people` 
ADD COLUMN `people_in` INT NOT NULL DEFAULT 9 ,
ADD COLUMN `people_out` INT NOT NULL DEFAULT 18 ;

ALTER TABLE `porteiro`.`people` 
ADD COLUMN `people_slack` VARCHAR(45) NULL AFTER `people_out`;

CREATE TABLE `porteiro`.`hash` (
  `hash_id` INT NOT NULL AUTO_INCREMENT,
  `hash_in` INT NOT NULL,
  `hash_out` INT NOT NULL,
  `hash_key` VARCHAR(64) NOT NULL,
  `hash_created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `hash_uses` INT NOT NULL DEFAULT 0,
  PRIMARY KEY (`hash_id`));

ALTER TABLE `porteiro`.`hash` 
CHANGE COLUMN `hash_key` `hash_key` VARCHAR(256) NOT NULL ;

ALTER TABLE `porteiro`.`hash` 
ADD COLUMN `people_id` INT NOT NULL AFTER `hash_uses`;


ALTER TABLE `porteiro`.`people` 
ADD COLUMN `people_hash` VARCHAR(512) NULL AFTER `people_out`;

ALTER TABLE `porteiro`.`people` 
ADD COLUMN `people_token` VARCHAR(512) NULL AFTER `people_out`;
