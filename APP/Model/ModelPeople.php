<?php

include_once('conf/db.php');

class ModelPeople extends db{

	// TODO: implementar visibilidade dos campos desta tabela com o array abaixo
	private $visible = [];

	function __construct(){
		parent::__construct();
	}

	public function add_people($data) {
		return db::insert('people', $data);
	}

	public function edit_people($data, $where) {
		return db::update('people', $data, $where);
	}

	public function get_by($data, $limit = 1) {
		return db::select('people', '*', $data, $limit);
	}

	public function get_by_id($id) {
		return db::select('people', '*', ['people_id' => $id], 1);
	}
	
	public function get_list($data) {
		return db::select('people', '*', $data, 20, 'people_id DESC');
	}

	public function get_count($data) {
		return db::count('people', $data);
	}
}