<?php

include_once('conf/db.php');

class ModelHash extends db{

	function __construct(){
		parent::__construct();
	}

	public function add_hash($data) {
		db::insert('hash', $data);
	}

	public function edit_hash($data, $where) {
		db::update('hash', $data, $where);
	}

	public function get_by($data, $limit = 1) {
		return db::select('hash', '*', $data, $limit);
	}

	public function get_last_by($data) {
		return db::select('hash', '*', $data, 1, 'hash_id DESC');
	}

	public function get_by_id($id) {
		return db::select('hash', '*', ['hash_id' => $id], 1);
	}
}