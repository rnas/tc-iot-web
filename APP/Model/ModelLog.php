<?php

include_once('conf/db.php');

class ModelLog extends db{

	function __construct(){
		parent::__construct();
	}

	public function add_log($data) {
		return db::insert('log', $data);
	}

	public function edit_log($data, $where) {
		return db::update('log', $data, $where);
	}

	public function get_by($data, $limit = 1) {
		return db::select('log join people on log.user_id = people.people_id', '*', $data, $limit);
	}

	public function get_by_id($id) {
		return db::select('log', '*', ['link_id' => $id], 1);
	}

	public function get_list($data = []) {
		return db::select('log join people on log.user_id = people.people_id', '*', $data, 10, 'log_id DESC');
	}

	public function get_count($data = []) {
		return db::count('log', $data);
	}
}