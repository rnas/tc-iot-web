<?php

include_once('conf/db.php');

class ModelStore extends db{

	function __construct(){
		parent::__construct();
	}

	public function add_store($data) {
		db::insert('store', $data);
	}

	public function edit_store($data, $where) {
		db::update('store', $data, $where);
	}

	public function get_by($data, $limit = 1) {
		db::select('store', '*', $data, $limit);
	}

	public function get_by_id($id) {
		db::select('store', '*', ['store_id' => $id], 1);
	}
}