<?php

include_once('conf/db.php');

class ModelUser extends db{

	function __construct(){
		parent::__construct();
	}

	public function create($data) {
		db::insert('user', $data);
		$last = db::lastRow('user', 'user_id');
		return $last;
	}

	public function edit_user($data, $where) {
		return db::update('user', $data, $where);
	}

	public function get_by($data, $limit = 1) {
		return db::select('user', '*', $data, $limit);
	}

	public function get_by_id($id) {
		return db::select('user', '*', ['user_id' => $id], 1);
	}

	public function userExists($data = false) {
		return (db::select('user', '*', $data));
	}

	public function get_list($data) {
		return db::select('user', '*', $data, 10, 'user_id DESC');
	}

	public function get_count($data) {
		return db::count('user', $data);
	}


}