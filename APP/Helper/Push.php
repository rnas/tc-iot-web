<?php

Class Push {

	private $APIKEY  = '....';
	private $APIURL  = 'https://fcm.googleapis.com/fcm/send';
	private $request = [];

	public function withContent($title = '', $description = '') {
	
		$this->request['notification']['id'] = 1;
		$this->request['notification']['body'] 	= $description;
		$this->request['notification']['title'] 	= $title;
	}

	public function sendTo($device) {
		$this->request['to'] = $device;
		$this->request['priority'] = 10;

		pr($this->request);

		$result = $this->submit();

		$result = json_decode($result);
		$result = $result->results[0];

		
	}

	private function submit() {

		$headr = array();
		$headr[] = 'Content-type: application/json';

		$headr[] = 'Authorization: key='.$this->APIKEY;	
		
		$push_options = json_encode($this->request);

		$headr[] = 'Content-Length: ' . strlen($push_options);

		$ch = curl_init();

		curl_setopt($ch, 	CURLOPT_POSTFIELDS, $push_options);
		curl_setopt($ch, 	CURLOPT_HTTPHEADER,$headr);
		curl_setopt($ch, 	CURLOPT_POST,true);
    	curl_setopt($ch, 	CURLOPT_URL, $this->APIURL);
	    curl_setopt($ch,	CURLOPT_ENCODING , "gzip");
		curl_setopt($ch, 	CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, 	CURLOPT_SSL_VERIFYPEER, false);

		$site = curl_exec($ch);
        curl_close($ch);

        return $site;
	}


}