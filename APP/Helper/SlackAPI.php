<?php 

Class SlackAPI {

	private $api_url = 'https://hooks.slack.com/services/...';


	public function request($data = false) {

		$ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->api_url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$data_string = json_encode($data);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);	
		curl_setopt($ch, CURLOPT_HTTPHEADER, [
			'Content-Type: application/json',
			'Content-Length: ' . strlen($data_string)
		]);
		
		$site = curl_exec($ch);
        curl_close($ch);

        return $site;

	}


	public function sendMessage($text, $emoji = ':no_entry_sign:', $username = '[admin-bot]', $channel = '#admin') {
		
		$data = [
			'channel' 	=> $channel,
			'text' 		=> $text,
			'username' 	=> $username,
			'icon_emoji' => $emoji,
			'mrkdwn' 	=> true,
		];

		$this->request($data);
	}


}