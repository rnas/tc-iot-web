<?php
class Session {

	public function set($name, $value){ $_SESSION[$name] = $value; }

	public function get($name, $or = false){ return (isset($_SESSION[$name])) ? $_SESSION[$name] : $or; }

	public function getAll(){ return $_SESSION; }

	public function remove($name){ unset($_SESSION[$name]); }

	public function removeAll(){ session_destroy(); }
	
}